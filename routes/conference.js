const path = require('path');

const express = require('express');
const router = express.Router();

//allConferences
router.get('/', (req, res, next) => {
    const conferences = ['Laravel', 'Nodejs', 'Sequelize'];
    res.render('conference/index', {
        path: '/conference',
        title: 'All conferences',
        description: 'What conferences are upcoming?',
        conferences: conferences
    });
});

//AddUser
router.get('/add-conference', (req, res, next) => {
    res.render('connference/add', {
        path: '/add-conference',
    });
});


//router.addUser = ();

module.exports = router;